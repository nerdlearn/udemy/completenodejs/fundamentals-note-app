console.log('Starting arrow-function.js');

var square = (x) => {
	var result = x * x;
	return result;
};
console.log(square(9));

//can be used for functions that just execute something and RETURN, leaving the return implicit
var squareSmaller = (x) => x * x;
console.log(squareSmaller(9));

//can be used for functions with ONE argument only
var squareSmallerNoParenthesis = x => x * x;
console.log(squareSmallerNoParenthesis(9));

var user = {
	name: 'Pedro',
	sayHi: () => {
		console.log(`Hi!`);
	},
	sayHiFailOnThis: () => {
		console.log(`Hi! I'm ${this.name}`);
	},
	sayHiAlt (){
		console.log(`Hi! I'm ${this.name}`);
	},
	sayHiArguments (){
		console.log(arguments),
		console.log(`Hi! I'm ${this.name}`);
	}
};

user.sayHi();
user.sayHiFailOnThis();
user.sayHiAlt();
user.sayHiArguments('BadaBing!', 'BadaBoom!');