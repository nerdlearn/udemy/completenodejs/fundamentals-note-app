console.log('Starting app.js');

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');
const notes = require('./notes.js');

const titleOptions = {
			describe: 'Title of note',
			demand: true, 
			alias: 't'
		};
const bodyOptions = {
			describe: 'Body of a note',
			demand: true, 
			alias: 'b'
		};

const argv = yargs
	.command('add', 'Add a new note', {
		title: titleOptions,
		body: bodyOptions,
	})
	.command('list', 'List all notes')
	.command('read', 'Read a single note', {
		title: titleOptions
	})
	.command('remove', 'Remove a single note', {
		title: titleOptions,
	})
	.help()
	.argv;

console.log('yargs.argv: ',argv);

console.log('process.argv: ',process.argv);

var command = process.argv[2];

console.log('Command: ',command);

if(command === 'add'){
	var note = notes.addNote(argv.title, argv.body);
	if(typeof note === "undefined"){
		console.log('Note was not added, check for duplicates');
	} else {
		console.log('Note got added.');
        notes.logNote(note);
	}
} else if (command === 'list'){
	var allNotes = notes.getAll();
	console.log(`Printing ${allNotes.length} notes`);
	allNotes.forEach((note) => notes.logNote(note));
} else if (command === 'read'){
	var note = notes.readNote(argv.title);
	if(typeof note === "undefined"){
		console.log('Note was not found.');
	} else {
		console.log('Note was found');
		notes.logNote(note);
	}
} else if (command === 'remove'){
	if(notes.removeNote(argv.title)){
		console.log('Note was removed');
	} else {
		console.log('Note was not removed');
	}
	
} else {
	console.log('Command not recognized');
}

/*

const os = require('os');

console.log(_.isString(true));
console.log(_.isString('true'));
console.log(_.uniq([2,1,2,3]));
var res = notes.addNote();
console.log('res is:', res);

var resAddition = notes.addition(7,-9);

console.log('resAddition is:', resAddition);

var user = os.userInfo();

fs.appendFile('greetings.txt',`Hello ${user.username}! you are ${notes.age}`, function (err){
	if(err){
		console.log('Unable to write to file');
	} 
}); 
*/